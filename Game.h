#include <iostream>
#include "ScoreCard.h"

using namespace std;

class Game {
	private:
	bool replay, win;
	public:
		Game(){ replay = true; win = false;}
		void Play();
		bool Restart();
		void Display(ScoreCard, int []);
		bool CheckEntry(int [], int);
};
