CC = g++
CFLAGS = -Wall
DEPS = Game.h dieNdice.h ScoreCard.h
OBJ = Main.o 

Main: Main.o Game.o dieNdice.o
	$(CC) Main.o Game.o dieNdice.o -g -o Main
Main.o: Main.cpp Game.cpp
	$(CC) $(CFLAGS) -c Main.cpp
Game.o: Game.cpp Game.h dieNdice.cpp ScoreCard.h
	$(CC) $(CFLAGS) -c Game.cpp
dieNdice.o: dieNdice.cpp dieNdice.h
	$(CC) $(CFLAGS) -c dieNdice.cpp
 
	$(CC) $(CFLAGS) -c Main.cpp

clean:
	rm -f *.o Main Game dieNdice